﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSound : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Bum());
    }

    IEnumerator Bum()
    {
        yield return new WaitForSeconds(0.3f);
        AudioManager.instance.Play("Explosion");
    }
}
