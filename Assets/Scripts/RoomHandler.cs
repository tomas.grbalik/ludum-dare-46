﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            //print(transform.gameObject.name);
            if(transform.GetChild(i).gameObject.name != "MainRoom")
                transform.GetChild(i).gameObject.SetActive(false);
        }
    }

}
