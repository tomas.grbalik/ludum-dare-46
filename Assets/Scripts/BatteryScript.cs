﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryScript : MonoBehaviour
{
    public float waitForTime = 1.5f;

    [Range(0,100)]
    public int charge = 100;

    private BoxCollider2D col;
    public void MakeNonPickableForStart()
    {
        col = GetComponent<BoxCollider2D>();
        col.enabled = false;
        StartCoroutine(ActivateCollider());
    }

    private IEnumerator ActivateCollider()
    {
        yield return new WaitForSeconds(waitForTime);
        col.enabled = true;
    }

    public Battery GetData()
    {
        return new Battery(charge, true);
    }

    public void PickedUp()
    {
        Destroy(gameObject);
    }
}
