﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public static GameManager instance;
    

    public bool isCounting = true;
    public float timeLeft = 25f;
    public float sliderValueThreshold = 20;
    public Image timeSlider;
    public Text timeLeftText;

    [Space] 
    public float chargeToTimeConstant = 2;

    [Space] 
    public PlayerController player;
    [Space]
    public GameObject batteryPrefab;

    [Space] 
    public GameObject currentRoom;
    public int enemiesToKill = 0;
    public GameObject TransitionPanel;
    public bool changingRooms = false;
    public bool leavingEnabled = true;

    [Space] 
    public Animator inventoryAnimator;
    public bool inventoryVisible = false;

    [Space] 
    public Transform canvas;
    public GameObject FlyInPanel;

    [Space] 
    public GameObject ultimateBattery;

    public GameObject teleport;
    public GameObject MainRoom;

    [Space] 
    public Light2D globalLight2D;

    public Vector2 lerpIntensity;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying this one!");
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isCounting)
        {
            timeLeft -= Time.deltaTime;

            if (timeLeft < sliderValueThreshold)
            {
                timeSlider.fillAmount = timeLeft / sliderValueThreshold;
                timeLeftText.text = Mathf.RoundToInt(timeLeft).ToString();

                globalLight2D.intensity = Mathf.Lerp(lerpIntensity.x, lerpIntensity.y, timeLeft / sliderValueThreshold);
            }
            else
            {
                timeLeftText.text = "";
                timeSlider.fillAmount = 1;
                globalLight2D.intensity = 1;
            }

            if (timeLeft <= 0)
            {
                GameManager.instance.GameLost();
                isCounting = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.E))
        {
            inventoryVisible = !inventoryVisible;
            inventoryAnimator.SetBool("Visible",inventoryVisible);
        }
    }

    public void OpenInv()
    {
        if (!inventoryVisible)
        {
            inventoryVisible = !inventoryVisible;
            inventoryAnimator.SetBool("Visible", inventoryVisible);
        }
    }

    public void CloseInv()
    {
        if (inventoryVisible)
        {
            inventoryVisible = !inventoryVisible;
            inventoryAnimator.SetBool("Visible", inventoryVisible);
        }
    }

    public void BatteryInserted(int charge)
    {
        float addedValue = charge / chargeToTimeConstant;
        timeLeft += addedValue;
        Debug.Log($"Battery inserted for {addedValue} for total of {timeLeft} time left.");
    }

    public void DropBattery(int charge)
    {
        BatteryScript bat = Instantiate(batteryPrefab, player.transform.position, Quaternion.identity)
            .GetComponent<BatteryScript>();
        bat.MakeNonPickableForStart();
        bat.charge = charge;
    }


    public void DoorHit(GameObject targetRoom, int targetSpawn)
    {
        if (!changingRooms && leavingEnabled)
        {
            if(targetRoom != null && targetSpawn>=1)
                StartCoroutine(ChangeRooms(targetRoom, targetSpawn));
        }
    }

    private IEnumerator ChangeRooms(GameObject targetRoom, int targetPosition)
    {
        changingRooms = true;
        TransitionPanel.SetActive(true);
        yield return new WaitForSeconds(0.40f);
        currentRoom.SetActive(false);
        currentRoom = targetRoom;
        currentRoom.SetActive(true);
        enemiesToKill = 0;
        enemiesToKill += currentRoom.GetComponentsInChildren<Enemy>().Length;
        enemiesToKill += currentRoom.GetComponentsInChildren<Ranger>().Length;
        enemiesToKill += currentRoom.GetComponentsInChildren<Boss>().Length;
        if (enemiesToKill > 0)
            leavingEnabled = false;

        if (enemiesToKill == 0)
            player.speed = 4.5f;
        else
        {
            player.speed = 3;
        }

        player.transform.position = targetRoom.transform.GetChild(targetPosition-1).position;
        Camera.main.transform.position = currentRoom.transform.position + new Vector3(0, 0, -10f);
        yield return new WaitForSeconds(0.55f);
        TransitionPanel.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        changingRooms = false;

    }

    public void Teleport()
    {
        StartCoroutine(TeleportCoroutine());
    }

    private IEnumerator TeleportCoroutine()
    {
        changingRooms = true;
        TransitionPanel.SetActive(true);
        yield return new WaitForSeconds(0.40f);
        currentRoom.SetActive(false);
        currentRoom = MainRoom;
        currentRoom.SetActive(true);
        enemiesToKill = 0;
        player.transform.position = currentRoom.transform.GetChild(0).position;
        Camera.main.transform.position = currentRoom.transform.position + new Vector3(0, 0, -10f);
        yield return new WaitForSeconds(0.55f);
        TransitionPanel.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        changingRooms = false;
    }

    public void EnemyKilled()
    {
        enemiesToKill--;
        if (enemiesToKill <= 0)
            leavingEnabled = true;
    }

    public void EnemyAdded()
    {
        enemiesToKill += 1;
    }

    public void DropPrize()
    {
        Vector2 pos = new Vector2(Random.Range(-2f, 0f), Random.Range(-2f, 2f));
        pos.x += currentRoom.transform.position.x;
        pos.y += currentRoom.transform.position.y;

        Instantiate(ultimateBattery, pos, Quaternion.identity);

        pos = new Vector2(Random.Range(0, 2f), Random.Range(-2f, 2f));
        pos.x += currentRoom.transform.position.x;
        pos.y += currentRoom.transform.position.y;

        Instantiate(teleport, pos, Quaternion.identity);
    }




    public void GameLost()
    {
        //Time.timeScale = 1;

        //LosePanel.SetActive(true);
        StartCoroutine(ResetGame());

    }

    private IEnumerator ResetGame()
    {
        Instantiate(FlyInPanel, canvas);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("MainScene");
    }

    public void GameWon()
    {
        StartCoroutine(FinishGame());
    }

    private IEnumerator FinishGame()
    {
        Instantiate(FlyInPanel, canvas);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("GameWon");
    }
}
