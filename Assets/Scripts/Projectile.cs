﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 4f;
    public int damage = 10;
    public float radius = 0.4f;

    public GameObject explosion;

    public Vector2 target;

    private bool moving;

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            Vector2 pos = transform.position;
            Vector2 dir = (target - pos).normalized;

            transform.Translate(dir*speed*Time.deltaTime);

            if(Vector2.Distance(transform.position,target) <= 0.08)
                Explode();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (!col.gameObject.CompareTag("Enemy"))
        {
            Explode();
        }
    }

    public void SetupAndFire(Vector2 target)
    {
        this.target = target;
        moving = true;
    }


    public void Explode()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);

        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius);
        foreach (var col in cols)
        {
            if(col.gameObject.CompareTag("Player"))
                col.gameObject.GetComponent<PlayerController>().ReceiveDamage(damage);
        }
        Destroy(gameObject);
    }
}
