﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    private Transform player;
    private Rigidbody2D rb;

    public float speed = 2f;
    public float maxHealth = 2f;
    public float health;
    public bool stunned = false;
    private float whenToUnstun;

    [Space]
    public float approachRadius = 0.8f;
    public float attackRadius = 1f;

    public float attackCooldown = 1f;
    public int damageOnHit = 1;

    [Space]
    public Image healthFill;

    [Space]
    public GameObject deathParticle;

    public GameObject enemy;
    public GameObject projectile;

    [HideInInspector]
    private Projectile proj;

    private bool readyToAttack;
    private float whenReadyToAttack;

    void Awake()
    {
        health = maxHealth;
        readyToAttack = true;
        whenReadyToAttack = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(SpecialEffects());
    }

    private IEnumerator SpecialEffects()
    {
        do
        {
            if(proj != null)
                proj.Explode();
            if (health > maxHealth / 2)
            {
                if (GameManager.instance.enemiesToKill < 5)
                {
                    //Spawn enemy
                    Vector2 pos = new Vector2(Random.Range(-5.5f, 5.5f), Random.Range(-4f, 4f));
                    pos.x += transform.parent.position.x;
                    pos.y += transform.parent.position.y;
                    Instantiate(enemy, pos, Quaternion.identity, transform.parent).GetComponent<Enemy>().SetHealth(4);
                    GameManager.instance.EnemyAdded();
                }

                yield return new WaitForSeconds(3f);
            }
            else
            {
                //SpawnParticle
                Vector2 pos = new Vector2(Random.Range(-5.5f, 5.5f), Random.Range(-4f, 4f));
                pos.x += transform.parent.position.x;
                pos.y += transform.parent.position.y;
                proj = Instantiate(projectile, pos, Quaternion.identity, transform.parent).GetComponent<Projectile>();

                yield return new WaitForSeconds(.75f);
            }
        } while (health>0);
    }

    void OnDestroy()
    {
        if(proj != null)
            proj.Explode();
        StopAllCoroutines();

        
    }

    // Update is called once per frame
    void Update()
    {
        if (!readyToAttack)
            if (whenReadyToAttack <= Time.timeSinceLevelLoad)
            {
                readyToAttack = true;
            }

        if (!stunned)
        {
            float dist = Vector2.Distance(player.position, transform.position);

            if (dist > approachRadius)
            {
                Vector2 dir = (player.position - transform.position).normalized;
                transform.Translate(dir * speed * Time.deltaTime);
            }

            if (dist < attackRadius && readyToAttack)
            {
                Attack();
            }
        }
        else if (whenToUnstun <= Time.timeSinceLevelLoad)
        {
            stunned = false;
        }


    }

    void Attack()
    {
        GetComponent<Animator>().SetTrigger("Attack");
        player.GetComponent<PlayerController>().ReceiveDamage(damageOnHit);
        readyToAttack = false;
        whenReadyToAttack = Time.timeSinceLevelLoad + attackCooldown;
    }

    public void DealDamage(int damage, float knockBack, float stunDuration)
    {
        health -= damage;
        healthFill.fillAmount = Mathf.Clamp01(health / maxHealth);
        Vector2 dirFromPlayer = (player.position - transform.position).normalized * (-1);

        rb.AddForce(dirFromPlayer * knockBack, ForceMode2D.Impulse);

        stunned = true;
        whenToUnstun = Time.timeSinceLevelLoad + stunDuration;

        if (health <= 0)
            Kill();
    }

    private void Kill()
    {
        GameManager.instance.EnemyKilled();
        Instantiate(deathParticle, transform);
        GameManager.instance.DropPrize();
        Destroy(gameObject, 0.5f);
    }
}

