﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    private Transform player;
    private Rigidbody2D rb;

    public float speed = 2f;
    public float maxHealth = 2f;
    public float health;
    public bool stunned = false;
    private float whenToUnstun;

    [Space]
    public float approachRadius = 0.8f;
    public float attackRadius = 1f;

    public float attackCooldown = 1f;
    public int damageOnHit = 1;

    [Space] 
    public Image healthFill;

    [Space] 
    public GameObject deathParticle;

    private bool readyToAttack;
    private float whenReadyToAttack;

    void Awake()
    {
        health = maxHealth;
        readyToAttack = true;
        whenReadyToAttack = 0;
    }

    // Start is called before the first frame update
    void Start() 
    { 
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!readyToAttack)
            if (whenReadyToAttack <= Time.timeSinceLevelLoad)
            {
                readyToAttack = true;
            }

        if (!stunned)
        {
            float dist = Vector2.Distance(player.position, transform.position);

            if (dist > approachRadius)
            {
                Vector2 dir = (player.position - transform.position).normalized;
                transform.Translate(dir * speed * Time.deltaTime);
            }

            if (dist < attackRadius && readyToAttack)
            {
                Attack();
            }
        }
        else if (whenToUnstun <= Time.timeSinceLevelLoad)
        {
            stunned = false;
        }


    }

    void Attack()
    {
        GetComponent<Animator>().SetTrigger("Attack");
        player.GetComponent<PlayerController>().ReceiveDamage(damageOnHit);
        readyToAttack = false;
        whenReadyToAttack = Time.timeSinceLevelLoad + attackCooldown;
    }

    public void DealDamage(int damage, float knockBack, float stunDuration)
    {
        health -= damage;
        healthFill.fillAmount = Mathf.Clamp01(health / maxHealth);
        Vector2 dirFromPlayer = (player.position - transform.position).normalized * (-1);

        rb.AddForce(dirFromPlayer * knockBack,ForceMode2D.Impulse);

        stunned = true;
        whenToUnstun = Time.timeSinceLevelLoad + stunDuration;

        if (health <= 0)
            Kill();
    }

    private void Kill()
    {
        GameManager.instance.EnemyKilled();
        Instantiate(deathParticle, transform);
        Destroy(gameObject,0.5f);
    }

    public void SetHealth(int health)
    {
        this.health = health;
        maxHealth = health;
    }
}
