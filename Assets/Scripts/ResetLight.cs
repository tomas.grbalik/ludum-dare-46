﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ResetLight : MonoBehaviour
{
    void OnDisable()
    {
        print("Resetting light");
        Light2D light = GetComponent<Light2D>();
        light.intensity = 1;
        light.pointLightOuterRadius = 3.3f;
        light.color = Color.white;
    }
}
