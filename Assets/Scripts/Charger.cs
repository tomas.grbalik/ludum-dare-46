﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Charger : MonoBehaviour
{
    public bool isEmpty = true;
    public bool isDone = true;

    public Sprite EmptyCharger;
    public Sprite FullCharger;

    public Image fill;

    public int batteryCharge;

    public float clickTime = 1f;
    private float timeNextClick; 

    public void AddBattery(int charge)
    {
        isEmpty = false;
        GetComponent<SpriteRenderer>().sprite = FullCharger;
        batteryCharge = charge;
        isDone = false;
        fill.fillAmount = batteryCharge / 100f;

        if (batteryCharge == 100)
            Finish();
        else
            timeNextClick = Time.timeSinceLevelLoad + clickTime;
    }

    void Update()
    {
        if (!isEmpty && !isDone)
        {
            if (timeNextClick <= Time.timeSinceLevelLoad)
            {
                batteryCharge += 1;
                fill.fillAmount = batteryCharge / 100f;

                if (batteryCharge == 100)
                    Finish();
                else
                    timeNextClick = Time.timeSinceLevelLoad + clickTime;
            }

        }
    }

    private void Finish()
    {
        isDone = true;
        fill.fillAmount = 1;
    }

    public int DropTheBattery()
    {
        GetComponent<SpriteRenderer>().sprite = EmptyCharger;

        isEmpty = true;
        isDone = false;
        fill.fillAmount = 1;

        return batteryCharge;
    }
}
