﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayerController : MonoBehaviour
{
    public int MaxHealth = 5;
    private int health = 5;
    public Image healthBar;
    public float speed;
    public BackpackController backpack;
    public List<Battery> batteries;
    public Transform targets;
    public bool hasUltimateBattery;
    public GameObject ultimateCanvas;
    public Animator lightStandAnimator;

    [Space] 
    public SpriteRenderer handle;
    public SpriteRenderer blade;

    [Space] 
    public bool hasBossKey = false;
    public GameObject bossKey;
    public StoneSequence bossStone;
    public bool nearBossStone = false;

    public bool nearChargerStone = false;
    public bool hasChargerKey=false;
    public GameObject chargerKey;
    public StoneSequence chargerStone;


    [Space] 
    public Sprite[] sprites;
    private int targetSprite;


    [Space] 
    public Transform attackPoint;
    public LayerMask enemyLayer;

    [Space] 
    public Sprite SwordSprite;
    public Sprite FlashLight;
    public Sprite Railgun;
    public Sprite Potion;
    public Sprite Teleport;
    public Sprite LockedButton;

    public Button SwordBut;
    public Button FlashlightBut;
    public Button RailgunButton;
    public Button PotionButton;
    public Button TeleportButton;
    public EventSystem eventSystem;

    [Space] 
    public Animator SaberAnimator;
    public bool saberEnabled = true;
    public float saberRadius = 1f;
    public int saberDamage = 1;
    public float saberKnockBack = 10f;
    public float saberStunEffect = 1f;
    public float saberCooldown = 0.3f;

    [Space]
    public Animator FlashLightAnimator;
    public bool flashLightEnabled = false;
    public Transform flashLightPoint1;
    public Transform flashLightPoint2;
    public int flashLightDamage = 5;
    public float flashLightKnockBack = 25f;
    public float flashLightStunEffect = 1f;
    public float flashLightCooldown = 1f;

    [Space]
    public Animator RailgunAnimator;
    public Transform railgunPoint;
    public bool railgunEnabled = false;
    public int railgunDamage = 5;
    public float railgunKnockBack = 25f;
    public float railgunStunEffect = 1f;
    public float railgunCooldown = 1.5f;

    [Space] 
    public GameObject potionEffect;
    public int potionCount;
    public int potionStrength = 3;
    public Text potionCountText;




    private Rigidbody2D rb;
    private Animator animator;


    private bool batteryInsertionPossible = false;
    private bool batteryChargingPossible = false;
    private Charger chargerNearby;
    private int freeSlotsLeft = 3;

    private bool readyToFire = true;
    private float whenReadyToFire;

    // Start is called before the first frame update
    void Start()
    {
        targetSprite = 0;
        health = MaxHealth;

        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        batteries = new List<Battery>();
        batteries.Add(new Battery(0, false));
        batteries.Add(new Battery(0, false));
        batteries.Add(new Battery(0, false));

        UnlockSword();
    }

    // Update is called once per frame
    void Update()
    {
        #region Movement

        float vertical = Input.GetAxisRaw("Vertical");
        float horizontal = Input.GetAxisRaw("Horizontal");
        //print(vertical + "  " + horizontal);


        Vector2 dir = new Vector2(horizontal, vertical);

        transform.Translate(dir.normalized * speed * Time.deltaTime);


        if (dir != Vector2.zero)
        {
            animator.enabled = true;
            animator.SetFloat("Horizontal", horizontal);
            animator.SetFloat("Vertical", vertical);
        }
        else
        {
            animator.enabled = false;
            GetComponent<SpriteRenderer>().sprite = sprites[targetSprite];
        }

        if (horizontal > 0.5)
        {
            targetSprite = 1;
            targets.localRotation = Quaternion.Euler(0,0,0);
            blade.sortingOrder = 11;
            handle.sortingOrder = 11;
        }
        else if (vertical > 0.5)
        {
            targetSprite = 0;
            targets.localRotation = Quaternion.Euler(0, 0, 90);
            blade.sortingOrder = 9;
            handle.sortingOrder = 9;
        }
        else if (vertical < -0.5)
        {
            targetSprite = 2;
            targets.localRotation = Quaternion.Euler(0, 0, -90);
            blade.sortingOrder = 11;
            handle.sortingOrder = 11;
        }
        else if (horizontal < -0.5)
        {
            targetSprite = 3;
            targets.localRotation = Quaternion.Euler(0, 0, 180);
            blade.sortingOrder = 9;
            handle.sortingOrder = 9;
        }

        #endregion

        #region Attack

        

        
        if (!readyToFire)
            if (whenReadyToFire <= Time.timeSinceLevelLoad)
                readyToFire = true;

        if (Input.GetKey(KeyCode.Space) && readyToFire)
        {
            if (saberEnabled)
            {
                SaberAttack();
            }
            else if (flashLightEnabled)
            {
                FlashlightAttack();
            }
            else if (railgunEnabled)
            {
                RailgunAttack();
            }
        }
        #endregion

        #region Charging

        

        
        if (batteryChargingPossible && Input.GetKeyDown(KeyCode.F))
        {
            if (!chargerNearby.isEmpty)
            {
                if (freeSlotsLeft > 0)
                {
                    int freeSlot = GetFreeSlot();
                    if (freeSlot >= 0)
                    {
                        Battery newBat = new Battery(chargerNearby.DropTheBattery(),true);
                        batteries[freeSlot] = newBat;
                        backpack.AddOnSlot(freeSlot, batteries[freeSlot].charge);
                        //print("Added new battery, now: " + (3-freeSlotsLeft + 1));

                        freeSlotsLeft--;
                        //print(freeSlotsLeft);
                    }
                }
            }
        }
        #endregion

        #region Stones

        if (nearChargerStone && Input.GetKeyDown(KeyCode.F))
        {
            if (hasChargerKey)
            {
                chargerStone.DestroySequence();
                nearChargerStone = false;
                hasChargerKey = false;
                chargerKey.SetActive(false);
            }
        }

        if (nearBossStone && Input.GetKeyDown(KeyCode.F))
        {
            if (hasBossKey)
            {
                bossStone.DestroySequence();
                nearBossStone = false;
                hasBossKey = false;
                bossKey.SetActive(false);
            }
        }
        #endregion

        if (hasUltimateBattery && batteryInsertionPossible && Input.GetKeyDown(KeyCode.F))
        {
            lightStandAnimator.SetTrigger("Insert");
            GameManager.instance.GameWon();
        }

        #region ForwardingKeysDown
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ExecuteEvents.Execute(SwordBut.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ExecuteEvents.Execute(FlashlightBut.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ExecuteEvents.Execute(RailgunButton.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ExecuteEvents.Execute(PotionButton.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ExecuteEvents.Execute(TeleportButton.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
        }
        #endregion

    }

    private void SaberAttack()
    {
        SaberAnimator.SetTrigger("Swing");

        readyToFire = false;
        whenReadyToFire = Time.timeSinceLevelLoad + saberCooldown;

        Collider2D[] cols = Physics2D.OverlapCircleAll(attackPoint.position, saberRadius, enemyLayer.value);
        
        foreach (var col in cols)
        {
            if (col.gameObject.CompareTag("Enemy"))
            {
                Enemy en = col.gameObject.GetComponent<Enemy>();
                Ranger rang = col.gameObject.GetComponent<Ranger>();
                if (en != null)
                {
                    en.DealDamage(saberDamage, saberKnockBack, saberStunEffect);
                }
                else if (rang != null)
                {
                    rang.DealDamage(saberDamage, saberKnockBack,saberStunEffect);
                }
                else
                {
                    col.gameObject.GetComponent<Boss>().DealDamage(saberDamage, saberKnockBack, saberStunEffect);
                }

            }
        }

        
    }

    private void FlashlightAttack()
    {
        FlashLightAnimator.SetTrigger("Shoot");
        StartCoroutine(FireFlashLight());
    }
    private IEnumerator FireFlashLight()
    {
        readyToFire = false;
        whenReadyToFire = Time.timeSinceLevelLoad + flashLightCooldown;
        yield return new WaitForSeconds(0.2f);
        AudioManager.instance.Play("Flash");
        yield return new WaitForSeconds(0.3f);
        Collider2D[] cols = Physics2D.OverlapAreaAll(flashLightPoint1.position, flashLightPoint2.position, enemyLayer);

        foreach (var col in cols)
        {
            if (col.gameObject.CompareTag("Enemy"))
            {
                Ranger rang = col.gameObject.GetComponent<Ranger>();
                Enemy en = col.gameObject.GetComponent<Enemy>();
                if (en != null)
                {
                    en.DealDamage(flashLightDamage, flashLightKnockBack, flashLightStunEffect);
                }
                else if (rang != null)
                {
                    rang.DealDamage(flashLightDamage, flashLightKnockBack, flashLightStunEffect);
                }
                else
                {
                    col.gameObject.GetComponent<Boss>()
                        .DealDamage(flashLightDamage, flashLightKnockBack, flashLightStunEffect);
                }
            }
        }
    }

    private void RailgunAttack()
    {
        StartCoroutine(FireRailgun());
    }
    private IEnumerator FireRailgun()
    {
        

        RailgunAnimator.SetTrigger("Shoot");
        readyToFire = false;
        whenReadyToFire = Time.timeSinceLevelLoad + railgunCooldown;
        yield return new WaitForSeconds(0.2f);
        AudioManager.instance.Play("Railgun");
        yield return new WaitForSeconds(0.18f);
        
        Vector2 dir = (railgunPoint.position - transform.position).normalized;
        RaycastHit2D[] hits = Physics2D.RaycastAll(
            transform.position, 
            dir, 
            15f,
            enemyLayer);

        foreach (var hit in hits)
        {
            if (hit.collider.gameObject.CompareTag("Enemy"))
            {
                Enemy en = hit.collider.gameObject.GetComponent<Enemy>();
                Ranger rang = hit.collider.gameObject.GetComponent<Ranger>();
                if (en != null)
                {
                    en.DealDamage(railgunDamage, railgunKnockBack, railgunStunEffect);
                }
                else if (rang!=null)
                {
                    rang.DealDamage(railgunDamage, railgunKnockBack, railgunStunEffect);
                }
                else
                {
                    hit.collider.gameObject.GetComponent<Boss>()
                        .DealDamage(railgunDamage, railgunKnockBack, railgunStunEffect);
                }
            }
        }
    }




    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Battery"))
        {
            if (freeSlotsLeft > 0)
            {
                int freeSlot = GetFreeSlot();
                if (freeSlot >= 0)
                {
                    Battery newBat = col.gameObject.GetComponent<BatteryScript>().GetData();
                    batteries[freeSlot] = newBat;
                    backpack.AddOnSlot(freeSlot, batteries[freeSlot].charge);
                    //print("Added new battery, now: " + (3-freeSlotsLeft + 1));

                    freeSlotsLeft--;
                    //print(freeSlotsLeft);

                    col.gameObject.GetComponent<BatteryScript>().PickedUp();
                    AudioManager.instance.Play("Pickup");
                }
            }
        }

        else if (col.gameObject.CompareTag("Insertion"))
        {
            batteryInsertionPossible = true;

            if (hasUltimateBattery)
            {
                ultimateCanvas.SetActive(true);
            }

            GameManager.instance.OpenInv();
            print("Battery Insertion Possible = true");
        }

        else if (col.gameObject.CompareTag("Charger"))
        {
            batteryChargingPossible = true;
            print("batteryChargingPossible = true");
            chargerNearby = col.gameObject.GetComponent<Charger>();
            print(chargerNearby);
            GameManager.instance.OpenInv();
        }

        else if (col.gameObject.CompareTag("Door"))
        {
            DoorData data = col.gameObject.GetComponent<DoorData>();
            GameManager.instance.DoorHit(data.targetRoom, data.targetSpawnPos);
        }

        else if (col.gameObject.CompareTag("FlashLight"))
        {
            AudioManager.instance.Play("Pickup");
            Destroy(col.gameObject);
            UnlockFlashlight();
        }

        else if (col.gameObject.CompareTag("Railgun"))
        {
            AudioManager.instance.Play("Pickup");
            Destroy(col.gameObject);
            UnlockRailgun();
        }

        else if (col.gameObject.CompareTag("BossKey"))
        {
            AudioManager.instance.Play("Pickup");
            hasBossKey = true;
            bossKey.SetActive(true);
            Destroy(col.gameObject);
        }

        else if (col.gameObject.CompareTag("ChargerKey"))
        {
            AudioManager.instance.Play("Pickup");
            hasChargerKey = true;
            chargerKey.SetActive(true);
            Destroy(col.gameObject);
        }

        else if (col.gameObject.CompareTag("ChargerStone"))
        {
            nearChargerStone = true;

        }

        else if (col.gameObject.CompareTag("BossStone"))
        {
            nearBossStone = true;
        }

        else if (col.gameObject.CompareTag("Potion"))
        {
            AudioManager.instance.Play("Pickup");
            potionCount += 1;
            potionCountText.text = potionCount.ToString();

            if(potionCount == 1)
                UnlockPotion();

            Destroy(col.gameObject);
        }

        else if (col.gameObject.CompareTag("Teleport"))
        {
            AudioManager.instance.Play("Pickup");
            Destroy(col.gameObject);
            UnlockTeleport();
        }

        else if (col.gameObject.CompareTag("UltBattery"))
        {
            AudioManager.instance.Play("Pickup");
            Destroy(col.gameObject);
            hasUltimateBattery = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Insertion"))
        {
            batteryInsertionPossible = false;

            if(hasUltimateBattery)
                ultimateCanvas.SetActive(false);

            GameManager.instance.CloseInv();
        }
        else if (col.gameObject.CompareTag("Charger"))
        {
            batteryChargingPossible = false;
            chargerNearby = null;
            print(chargerNearby);
        }
        else if (col.gameObject.CompareTag("ChargerStone"))
        {
            nearChargerStone = false;
        }
        else if (col.gameObject.CompareTag("BossStone"))
        {
            nearBossStone = false;

        }
    }







    public void RemoveAtSlot(int slot)
    {
        if (batteryInsertionPossible)
        {
            lightStandAnimator.SetTrigger("Insert");
            GameManager.instance.BatteryInserted(batteries[slot].charge);

            AudioManager.instance.Play("Insert");
        }
        else if (chargerNearby)
        {
            if (chargerNearby.isEmpty)
                chargerNearby.AddBattery(batteries[slot].charge);
        }
        else
        {
            GameManager.instance.DropBattery(batteries[slot].charge);
        }

        freeSlotsLeft += 1;
        batteries[slot] = new Battery(0, false);
    }

    private int GetFreeSlot()
    {
        if (!batteries[0].isThere)
            return 0;
        else if (!batteries[1].isThere)
            return 1;
        else if (!batteries[2].isThere)
            return 2;

        return -1;
    }





    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(attackPoint.position, saberRadius);
    }

    public void ReceiveDamage(int value)
    {
        health -= value;
        //print($"Received {value} points of damage. Now at: {health}");

        healthBar.fillAmount = health / (float) MaxHealth;

        if (health <= 0)
        {
            GameManager.instance.GameLost();
        }
    }

    private void Heal()
    {

        health += potionStrength;
        health = Mathf.Clamp(health, 0, MaxHealth);
        healthBar.fillAmount = health / (float)MaxHealth;
        Instantiate(potionEffect, transform);
    }



    public void UnlockSword()
    {
        SwordBut.image.sprite = SwordSprite;
        SwordBut.interactable = true;
        ExecuteEvents.Execute(SwordBut.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
    }

    public void UnlockFlashlight()
    {
        FlashlightBut.image.sprite = FlashLight;
        FlashlightBut.interactable = true;
    }

    public void UnlockRailgun()
    {
        RailgunButton.image.sprite = Railgun;
        RailgunButton.interactable = true;
    }

    public void UnlockPotion()
    {
        PotionButton.image.sprite = Potion;
        PotionButton.interactable = true;
        potionCountText.gameObject.SetActive(true);
        potionCountText.text = potionCount.ToString();
    }

    public void UnlockTeleport()
    {
        TeleportButton.image.sprite = Teleport;
        TeleportButton.interactable = true;
    }


    public void LockPotion()
    {
        PotionButton.image.sprite = LockedButton;
        PotionButton.interactable = false;
        potionCountText.gameObject.SetActive(false);
        potionCountText.text = "";
    }


    public void SwordClick()
    {
        saberEnabled = true;
        flashLightEnabled = false;
        railgunEnabled = false;
    }

    public void FlashLightClick()
    {
        saberEnabled = false;
        flashLightEnabled = true;
        railgunEnabled = false;
    }

    public void RailgunClick()
    {
        saberEnabled = false;
        flashLightEnabled = false;
        railgunEnabled = true;
    }

    public void PotionClick()
    {
        if (potionCount > 0)
        {
            potionCount--;
            potionCountText.text = potionCount.ToString();
            if (potionCount == 0)
            {
                LockPotion();
            }

            Heal();
        }
    }

    public void TeleportClick()
    {
        GameManager.instance.Teleport();
    }

}

public struct Battery
{
    public int charge;
    public bool isThere;

    public Battery(int Charge, bool isThere)
    {
        charge = Charge;
        this.isThere = isThere;
    }
}
