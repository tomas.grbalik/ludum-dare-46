﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject FlyInPanel;

    public GameObject tutorial;
    public GameObject[] toDisable;

    public GameObject[] images;
    private int selected = 0;

    public void StartClick()
    {
        for (int i = 0; i < toDisable.Length; i++)
        {
            toDisable[i].SetActive(false);
        }
        tutorial.SetActive(true);
    }

    public void Continue()
    {
        selected += 1;
        if (selected >= 7)
        {
            TutorialFinishClick();
            return;
        }

        for (int i = 0; i < images.Length; i++)
        {
            images[i].SetActive(false);
        }
        images[selected].SetActive(true);
    }


    public void TutorialFinishClick()
    {
        Instantiate(FlyInPanel, transform);
        StartCoroutine(SwitchToGame());
    }


    private IEnumerator SwitchToGame()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("MainScene");
    }

    public void BackToMenu()
    {
        Instantiate(FlyInPanel, transform);
        StartCoroutine(SwitchToMenu());
    }

    private IEnumerator SwitchToMenu()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Menu");
    }
}
