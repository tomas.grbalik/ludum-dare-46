﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneSequence : MonoBehaviour
{
    public void DestroySequence()
    {
        transform.GetChild(1).gameObject.SetActive(true);
        BoxCollider2D[] cols = GetComponents<BoxCollider2D>();

        foreach (var col in cols)
        {
            col.enabled = false;
        }

        GetComponent<SpriteRenderer>().enabled = false;

        Destroy(gameObject,.75f);
    }
}
