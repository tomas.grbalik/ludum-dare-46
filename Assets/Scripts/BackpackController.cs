﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackpackController : MonoBehaviour
{
    public GameObject[] bats;

    public Image[] charges;

    public void AddOnSlot(int slot, int charge)
    {
        bats[slot].SetActive(true);
        bats[slot].transform.GetChild(0).GetComponent<Text>().text = charge.ToString();
        charges[slot].gameObject.SetActive(true);
        charges[slot].transform.parent.gameObject.SetActive(true);
        charges[slot].fillAmount = charge/100f;
    }

    public void RemoveAt(int slot)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().RemoveAtSlot(slot);

        bats[slot].SetActive(false);
        charges[slot].gameObject.SetActive(false);
        charges[slot].transform.parent.gameObject.SetActive(false);
    }
}
